﻿using FizzBuzz.Business.Interface;
using FizzBuzz.Business.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FizzBuzz.Business.Tests.Rules
{
    [TestClass]
    public class FizzRuleTests
    {
        [TestMethod]
        public void IsMatched_WhenRuleIsMatched_ReturnsTrue()
        {
            var wizzWuzzRuleMock = GetWednesdayRuleMock(false);
            var service = new FizzRule(wizzWuzzRuleMock.Object);

            var result = service.IsMatched(3);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsMatched_WhenRuleIsNotMatched_ReturnsFalse()
        {
            var wizzWuzzRuleMock = GetWednesdayRuleMock(false);
            var service = new FizzRule(wizzWuzzRuleMock.Object);

            var result = service.IsMatched(5);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DisplayString_WhenWednesday_ReturnsWizzString()
        {
            var wizzWuzzRuleMock = GetWednesdayRuleMock(true);
            var service = new FizzRule(wizzWuzzRuleMock.Object);

            var result = service.DisplayString();

            Assert.AreEqual(result, "Wizz");
        }

        [TestMethod]
        public void DisplayString_WhenNotWednesday_ReturnsBuzzString()
        {
            var wizzWuzzRuleMock = GetWednesdayRuleMock(false);
            var service = new FizzRule(wizzWuzzRuleMock.Object);

            var result = service.DisplayString();

            Assert.AreEqual(result, "Fizz");
        }

        private Mock<IDayCheck> GetWednesdayRuleMock(bool isWednesday)
        {
            var wizzWuzzRuleMock = new Mock<IDayCheck>();
            wizzWuzzRuleMock.Setup(x => x.IsWednesday()).Returns(isWednesday);
            return wizzWuzzRuleMock;
        }
    }
}
