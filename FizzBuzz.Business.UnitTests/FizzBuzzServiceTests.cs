﻿using FizzBuzz.Business.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz.Business.Tests
{
    [TestClass]
    public class FizzBuzzServiceTests
    {
        [TestMethod]
        public void GetRecordSet_WithNoLimit_ReturnsEmpty()
        {
            var emptyList = new List<string>();
            var ruleCheckerMock = new Mock<IRuleChecker>();
            var service = new FizzBuzzService(ruleCheckerMock.Object);

            var records = service.GetRecordSet(0);

            Assert.AreEqual(records.Count, emptyList.Count);
        }

        [TestMethod]
        public void GetRecordSet_WithNoRule_ReturnsTheSameNumbers()
        {
            var range = 3;
            var expactedResult = new List<string> { "1", "2", "3" };
            var ruleCheckerMock = new Mock<IRuleChecker>();
            var service = new FizzBuzzService(ruleCheckerMock.Object);

            var records = service.GetRecordSet(range);

            Assert.AreEqual(records.Count, range);
            CollectionAssert.AreEqual(records.ToList(), expactedResult);
        }

        [TestMethod]
        public void GetRecordSet_WithFizzRule_ReturnsFizzResult()
        {
            var range = 3;
            var fizzBuzzRuleMock = CreateRuleMock(3);
            var expactedResult = new List<string> { "1", "2", "Fizz" };
            var ruleCheckerMock = new Mock<IRuleChecker>();
            ruleCheckerMock.Setup(p => p.GetMatchedRules(3)).Returns(fizzBuzzRuleMock);
            var service = new FizzBuzzService(ruleCheckerMock.Object);

            var records = service.GetRecordSet(range);

            Assert.AreEqual(records.Count, range);
            CollectionAssert.AreEqual(records.ToList(), expactedResult);
        }

        [TestMethod]
        public void GetRecordSet_WithBuzzRule_ReturnsBuzzResult()
        {
            var range = 5;
            var fizzBuzzRuleMock = CreateRuleMock(5);
            var expactedResult = new List<string> { "1", "2", "3", "4", "Buzz" };
            var ruleCheckerMock = new Mock<IRuleChecker>();
            ruleCheckerMock.Setup(p => p.GetMatchedRules(5)).Returns(fizzBuzzRuleMock);
            var service = new FizzBuzzService(ruleCheckerMock.Object);

            var records = service.GetRecordSet(range);

            Assert.AreEqual(records.Count, range);
            CollectionAssert.AreEqual(records.ToList(), expactedResult);
        }

        [TestMethod]
        public void GetRecordSet_WithFizzBuzzRule_ReturnsFizzBuzzResult()
        {
            var range = 5;
            var expactedResult = new List<string> { "1", "2", "Fizz", "4", "Buzz" };
            var buzzRuleMock = CreateRuleMock(5);
            var fizzRuleMock = CreateRuleMock(3);
            var ruleCheckerMock = new Mock<IRuleChecker>();
            ruleCheckerMock.Setup(p => p.GetMatchedRules(3)).Returns(fizzRuleMock);
            ruleCheckerMock.Setup(p => p.GetMatchedRules(5)).Returns(buzzRuleMock);
            var service = new FizzBuzzService(ruleCheckerMock.Object);

            var records = service.GetRecordSet(range);

            Assert.AreEqual(records.Count, range);
            CollectionAssert.AreEqual(records.ToList(), expactedResult);
        }

        [TestMethod]
        public void GetRecordSet_WithiWzzWuzzRule_ReturnsWizzWuzzResult()
        {
            var range = 5;
            var expactedResult = new List<string> { "1", "2", "Wizz", "4", "Wuzz" };
            var buzzRuleMock = CreateRuleMock(5, true);
            var fizzRuleMock = CreateRuleMock(3, true);
            var ruleCheckerMock = new Mock<IRuleChecker>();
            ruleCheckerMock.Setup(p => p.GetMatchedRules(3)).Returns(fizzRuleMock);
            ruleCheckerMock.Setup(p => p.GetMatchedRules(5)).Returns(buzzRuleMock);
            var service = new FizzBuzzService(ruleCheckerMock.Object);

            var records = service.GetRecordSet(range);

            Assert.AreEqual(records.Count, range);
            CollectionAssert.AreEqual(records.ToList(), expactedResult);
        }

        private IList<IFizzBuzzRule> CreateRuleMock(int number, bool isWizzWuzz = false)
        {
            var rules = new List<IFizzBuzzRule>();
            var isWednesday = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday || isWizzWuzz;

            if (number % 3 == 0)
            {
                var fizzRuleMock = new Mock<IFizzBuzzRule>();
                fizzRuleMock.Setup(p => p.IsMatched(number)).Returns(true);
                fizzRuleMock.Setup(p => p.DisplayString()).Returns(isWednesday ? "Wizz" : "Fizz");
                rules.Add(fizzRuleMock.Object);
            }
            if (number % 5 == 0)
            {
                var buzzRuleMock = new Mock<IFizzBuzzRule>();
                buzzRuleMock.Setup(p => p.IsMatched(number)).Returns(true);
                buzzRuleMock.Setup(p => p.DisplayString()).Returns(isWednesday ? "Wuzz" : "Buzz");
                rules.Add(buzzRuleMock.Object);
            }
            return rules;
        }
    }
}