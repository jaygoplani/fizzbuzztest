using FizzBuzz.Business;
using FizzBuzz.Business.Interface;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace FizzBuzz.Web.Tests
{
    public static class UnityConfig
    {
        public static UnityContainer RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IFizzBuzzService, FizzBuzzService>();
            container.RegisterType<IRuleChecker, RuleChecker>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }
    }
}