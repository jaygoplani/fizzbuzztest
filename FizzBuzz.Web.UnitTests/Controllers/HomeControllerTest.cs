﻿using FizzBuzz.Business.Interface;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Unity;

namespace FizzBuzz.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index_WhenModelStateIsValid_ReturnsValidRecordSet()
        {
            var expactedResultPage = new PagedList<string>(new List<string> { "1", "2", "Fizz" }, 1, 3);
            var container = UnityConfig.RegisterComponents();
            var fizzBuzzService = container.Resolve<IFizzBuzzService>();
            HomeController controller = new HomeController(fizzBuzzService);
            var fizzBuzzViewModel = new FizzBuzzViewModel { InputNumber = 10 };
            fizzBuzzViewModel.InputNumber = 10;

            ViewResult result = controller.Index(fizzBuzzViewModel, 1) as ViewResult;
            var model = result.Model as FizzBuzzViewModel;

            Assert.AreEqual(model.Records.TotalItemCount, fizzBuzzViewModel.InputNumber);
            Assert.AreEqual(model.Records.Count, 3);
            CollectionAssert.AreEqual(model.Records.ToList<string>(), expactedResultPage.ToList<string>());
        }

        [TestMethod]
        public void Index_WhenModelStateIsInvalid_ReturnsEmptyRecordSet()
        {
            var container = UnityConfig.RegisterComponents();
            var fizzBuzzService = container.Resolve<IFizzBuzzService>();
            HomeController controller = new HomeController(fizzBuzzService);
            var fizzBuzzViewModel = new FizzBuzzViewModel { InputNumber = -2 };

            ViewResult result = controller.Index(fizzBuzzViewModel, 1) as ViewResult;
            var model = result.Model as FizzBuzzViewModel;

            Assert.AreEqual(model.Records.TotalItemCount, 0);
        }
    }
}