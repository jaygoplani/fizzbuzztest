﻿using FizzBuzz.Business.Interface;
using FizzBuzz.Business.Rules;
using System.Collections.Generic;

namespace FizzBuzz.Business
{
    public class RuleChecker : IRuleChecker
    {
        private readonly IList<IFizzBuzzRule> _fizzBuzzRules;
        private readonly IDayCheck _wizzWuzzRule;

        public RuleChecker()
        {
            //Initialize Rules 
            _wizzWuzzRule = new DayCheck();
            _fizzBuzzRules = new List<IFizzBuzzRule> { new FizzRule(_wizzWuzzRule), new BuzzRule(_wizzWuzzRule) };
        }

        /// <summary>
        /// This method is used to get the matched rules
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public IList<IFizzBuzzRule> GetMatchedRules(int number)
        {
            var matchedRules = new List<IFizzBuzzRule>();
            foreach (var rule in _fizzBuzzRules)
            {
                if (rule.IsMatched(number))
                {
                    matchedRules.Add(rule);
                }
            }
            return matchedRules;
        }
    }
}
