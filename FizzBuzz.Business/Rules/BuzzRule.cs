﻿using FizzBuzz.Business.Interface;

namespace FizzBuzz.Business.Rules
{
    public class BuzzRule : IFizzBuzzRule
    {
        private readonly IDayCheck _wizzWuzzRule;
        public BuzzRule(IDayCheck wizzWuzzRule)
        {
            _wizzWuzzRule = wizzWuzzRule;
        }

        public string DisplayString()
        {
            return _wizzWuzzRule.IsWednesday() ? "Wuzz" : "Buzz";
        }
        public bool IsMatched(int number)
        {
            return number % 5 == 0;
        }
    }
}
