﻿namespace FizzBuzz.Business.Interface
{
    public interface IFizzBuzzRule
    {
        string DisplayString();
        bool IsMatched(int number);
    }
}
