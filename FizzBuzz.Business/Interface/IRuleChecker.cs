﻿using System.Collections.Generic;

namespace FizzBuzz.Business.Interface
{
    public interface IRuleChecker
    {
        IList<IFizzBuzzRule> GetMatchedRules(int number);
    }
}