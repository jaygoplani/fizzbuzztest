﻿using System.Collections.Generic;

namespace FizzBuzz.Business.Interface
{
    public interface IFizzBuzzService
    {
        IList<string> GetRecordSet(int number);
    }
}