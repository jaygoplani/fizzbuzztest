﻿namespace FizzBuzz.Business.Interface
{
    public interface IDayCheck
    {
        bool IsWednesday();
    }
}