﻿using FizzBuzz.Business.Interface;
using FizzBuzz.Web.Models;
using PagedList;
using System.Web.Mvc;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        public const int PageSize = 3;

        private readonly IFizzBuzzService _fizzBuzzService;

        public HomeController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
        }

        public ActionResult Index(FizzBuzzViewModel fizzBuzzViewModel, int? page)
        {
            int pageNumber = page ?? 1;

            if (ModelState.IsValid)
            {
                fizzBuzzViewModel.Records = (PagedList<string>)_fizzBuzzService.GetRecordSet(fizzBuzzViewModel.InputNumber).ToPagedList(pageNumber, PageSize);
            }
            return View(model: fizzBuzzViewModel);
        }
    }
}
