using FizzBuzz.Business;
using FizzBuzz.Business.Interface;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace FizzBuzz.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IFizzBuzzService, FizzBuzzService>();
            container.RegisterType<IRuleChecker, RuleChecker>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}